package photosearch.heshamfas.com.photosearchapplication.communicator;

import android.support.annotation.NonNull;
import android.util.Log;

import java.util.Map;

import photosearch.heshamfas.com.photosearchapplication.data.PhotoItem;
import photosearch.heshamfas.com.photosearchapplication.data.PhotoResponseRepoInjector;
import photosearch.heshamfas.com.photosearchapplication.data.PhotoSearchResponse;
import photosearch.heshamfas.com.photosearchapplication.service.ConnectivityMonitorReceiver;
import photosearch.heshamfas.com.photosearchapplication.service.PhotoRepoInjectorCallBack;
import photosearch.heshamfas.com.photosearchapplication.service.Utils.UrlUtil;


/**
 * Created by Hesham on 3/11/2017.
 */

public class PhotoSearchPresenter implements PhotoSearchContract.UserActionsListener,
        PhotoRepoInjectorCallBack, ConnectivityMonitorReceiver.ConnectivityChangedListener{
    private final PhotoSearchContract.View mPhotoView ;
    private boolean mIsConnected;

    public PhotoSearchPresenter(@NonNull PhotoSearchContract.View photoView) {
        mPhotoView = photoView;
        mIsConnected = ConnectivityMonitorReceiver.isConnected();
        ConnectivityMonitorReceiver.addListener(this);
    }

    @Override
    public void loadPhotoItems(boolean forceUpdate, Map<String,String> params) {
        if(mIsConnected && forceUpdate){
                    new PhotoResponseRepoInjector(this).launchPhotoSearchServiceCall(UrlUtil.getUrl(params));
            mPhotoView.setProgressIndicator(true);
                }else {
            mPhotoView.setProgressIndicator(false);
        }
    }
    @Override
    public void onPhotoRepoReceived(PhotoSearchResponse repository) {
        if(repository.isReady()){
            mPhotoView.showPhotos(repository.getPhotoItems());
        }else {
            mPhotoView.showEmptyPhotos();
        }
            mPhotoView.setProgressIndicator(false);
    }
    @Override
    public void openPhotoDetails(@NonNull PhotoItem requestedPhotoItem) {
	    Log.d(this.getClass().getSimpleName(),"getting Photo Details" + requestedPhotoItem.getPhotoId());
	    mPhotoView.showPhotoDetailUi(requestedPhotoItem);
    }
    @Override
    public void onConnectivityChanged(boolean isConnected) {
        if(!isConnected){
            mIsConnected=false;
            mPhotoView.showEmptyPhotos();
        }else {
            mIsConnected = true;
           // loadPhotoItems(true); //ToDo // FIXME: 3/12/2017
        }
    }
}
