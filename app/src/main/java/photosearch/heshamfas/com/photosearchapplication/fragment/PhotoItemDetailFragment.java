package photosearch.heshamfas.com.photosearchapplication.fragment;

import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

import photosearch.heshamfas.com.photosearchapplication.PhotoSearchApplication;
import photosearch.heshamfas.com.photosearchapplication.R;

/**
 * Created by Hesham on 3/12/2017.
 */
public class PhotoItemDetailFragment extends DialogFragment implements
        View.OnClickListener{
    ImageLoader mImageLoader = PhotoSearchApplication.getInstance().getImageLoader();
    NetworkImageView mNetworkImageView;
    private String mImageUrl;
    private Button mCloseBtn;
	private static final String URL = "url";
    
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        return View.inflate(getActivity(), R.layout.photo_detail_dialog_fragment,container);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mNetworkImageView = (NetworkImageView)view.findViewById(R.id.niv_detail_picture);
        mCloseBtn = (Button) view.findViewById(R.id.btn_detail_close);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getDialog().setCanceledOnTouchOutside(false);
        Bundle arguments = getArguments();
        mImageUrl = arguments.getString(URL);
        init();
}

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
}

    private void init(){
	    mCloseBtn.setOnClickListener(this);
        mNetworkImageView.setDefaultImageResId(R.drawable.downloading);
        if(!TextUtils.isEmpty(mImageUrl)) {
            mNetworkImageView.setImageUrl(mImageUrl, mImageLoader);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_detail_close:
            	dismiss();
                break;
        }
    }
}

