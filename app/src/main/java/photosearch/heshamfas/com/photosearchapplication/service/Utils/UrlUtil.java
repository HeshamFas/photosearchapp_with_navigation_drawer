package photosearch.heshamfas.com.photosearchapplication.service.Utils;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Iterator;
import java.util.Map;

import photosearch.heshamfas.com.photosearchapplication.PhotoSearchApplication;

/**
 * Created by Hesham on 3/12/2017.
 */

public class UrlUtil {
	private static String UMBERSAND = "&";
	private static String EQUAL = "=";
	private static String Q_MARK = "?";
	public static String getUrl(Map<String,String> params){
		StringBuilder builder = new StringBuilder();
		builder.append(getBaseURL());
		builder.append(Q_MARK);
		Iterator<Map.Entry<String,String>> iterator  = params.entrySet().iterator();
		while (iterator.hasNext()){
			Map.Entry<String,String> pair = iterator.next();
			builder.append(pair.getKey());
			builder.append(EQUAL);
			builder.append(pair.getValue());
			builder.append(UMBERSAND);
		}
		//URL url = new URL("https://pixabay.com/api/?key=4778077-1fd32cae6f6399fcd78006ddc&q=yellow+flowers&image_type=photo&per_page=200");
	return builder.toString();
	}
	
	public static String encodeSearchCriteria(String searchString) {
		String encodedString= "";
		try {
			encodedString = URLEncoder.encode(searchString,"UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return encodedString;
	}
	private static String getBaseURL(){
		return PhotoSearchApplication.getInstance().getMainSearchURL();
	}
}
