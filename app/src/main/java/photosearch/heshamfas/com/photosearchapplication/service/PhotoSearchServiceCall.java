package photosearch.heshamfas.com.photosearchapplication.service;

import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.util.Log;
import org.json.JSONException;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import photosearch.heshamfas.com.photosearchapplication.data.PhotoSearchResponse;

/**
 * Created by Hesham on 3/11/2017.
 */
public class PhotoSearchServiceCall extends AsyncTask<String,Void, PhotoSearchResponse> {

    private PhotoSearchServiceAPI mPhotoSearchServiceAPI;

    public PhotoSearchServiceCall(@NonNull PhotoSearchServiceAPI promotionServiceAPI){
        this.mPhotoSearchServiceAPI = promotionServiceAPI;
    }
    
    @Override
    protected PhotoSearchResponse doInBackground(String... params) {
        Log.d("Hello", this.toString() + "do in the background");
        PhotoSearchResponse output = null;
        HttpURLConnection conn = null;
        try {
            URL url = new URL(params[0]);
            //URL url = new URL("https://pixabay.com/api/?key=4778077-1fd32cae6f6399fcd78006ddc&q=yellow+flowers&image_type=photo&per_page=200");
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");
            conn.connect();
            InputStream is = conn.getInputStream();
            output = PhotoSearchParser.parse(is);
            conn.disconnect();
        } catch (IOException e) {
            Log.d("Hello", this.toString() + "do in the background IO Exception");
            e.printStackTrace();
        } catch (JSONException e) {
            Log.d("Hello", this.toString() + "do in the background JSON Exception");
            e.printStackTrace();
        }finally {
            return output;
        }
    }

    @Override
    protected void onPostExecute(PhotoSearchResponse photoSearchResponse) {
        super.onPostExecute(photoSearchResponse);
            if(photoSearchResponse!= null /*&& photoSearchResponse.isReady()*/){
                mPhotoSearchServiceAPI.onSuccess(photoSearchResponse);
            }else {
                mPhotoSearchServiceAPI.onFailure();
            }
    }
}
