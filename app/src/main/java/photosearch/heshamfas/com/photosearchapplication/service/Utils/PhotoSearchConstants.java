package photosearch.heshamfas.com.photosearchapplication.service.Utils;

/**
 * Created by Hesham on 3/12/2017.
 */

public class PhotoSearchConstants {
	public static final String BASE_URL="base_url";
	public static final String KEY = "key";
	public static final String QUERY = "q";
	public static final String IMAGE_TYPE = "image_type";
	public static final String PER_PAGE = "per_page";
	public static final String PAGE = "page";
}
