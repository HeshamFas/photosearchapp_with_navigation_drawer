package photosearch.heshamfas.com.photosearchapplication.service;


import photosearch.heshamfas.com.photosearchapplication.data.PhotoSearchResponse;

/**
 * Created by Hesham on 3/11/2017.
 */
public interface PhotoRepoInjectorCallBack {
    void onPhotoRepoReceived(PhotoSearchResponse repository);
}
