package photosearch.heshamfas.com.photosearchapplication.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;
import java.util.HashMap;
import java.util.List;
import photosearch.heshamfas.com.photosearchapplication.R;
import photosearch.heshamfas.com.photosearchapplication.activity.PhotoSearchActivity;
import photosearch.heshamfas.com.photosearchapplication.communicator.PhotoSearchContract;
import photosearch.heshamfas.com.photosearchapplication.communicator.PhotoSearchPresenter;
import photosearch.heshamfas.com.photosearchapplication.communicator.SideMenuItemClickedListener;
import photosearch.heshamfas.com.photosearchapplication.contents.PhotoSearchAdapter;
import photosearch.heshamfas.com.photosearchapplication.contents.TwoWayLayoutManager;
import photosearch.heshamfas.com.photosearchapplication.data.PhotoItem;
import photosearch.heshamfas.com.photosearchapplication.service.Utils.PhotoSearchConstants;

import static photosearch.heshamfas.com.photosearchapplication.fragment.PhotoSearchActivityFragment.LayoutManagerType.GRID;
import static photosearch.heshamfas.com.photosearchapplication.fragment.PhotoSearchActivityFragment.LayoutManagerType.LINEAR;
import static photosearch.heshamfas.com.photosearchapplication.fragment.PhotoSearchActivityFragment.LayoutManagerType.TWOWAY;


/**
 * A placeholder fragment containing a simple view.
 */
public class PhotoSearchActivityFragment extends Fragment implements PhotoSearchContract.View,SideMenuItemClickedListener{

	private RecyclerView mRecyclerView;
	private ProgressBar mProgressBar;
	private PhotoSearchPresenter mPresenter;
	private static final String URL = "url";
	private String mRefreshMessage;
	private String mSearchQuery ="flower";
    private int mCurrentPage =1 ;
    private int mPerPage= 150;
	private List<PhotoItem> mPhotoItems;
	private LayoutManagerType mLayoutManagerType = LayoutManagerType.GRID;
    private static final String FLOWER = "flower";
    private static final String CAR = "car";
    private static final String BUG = "bug";
    private static final String PHOTO = "photo";
	public PhotoSearchActivityFragment() {
	}

	public enum LayoutManagerType{
		GRID, TWOWAY,LINEAR
	}
	
	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	                         Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_photo_search, container, false);
	}
	
	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		mProgressBar = (ProgressBar)view.findViewById(R.id.pb_photo_loading) ;
		mRecyclerView =(RecyclerView)view.findViewById(R.id.rv_search_results);
		mRefreshMessage = getResources().getString(R.string.fab_refresh_message);
		FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.fab);
		fab.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				mPresenter.loadPhotoItems(true, getUrlParams());
			}
		});
	}
	
	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		mPresenter = new PhotoSearchPresenter(this);
		mPresenter.loadPhotoItems(true, getUrlParams());
		((PhotoSearchActivity)getActivity()).setNavigationItemClickedListner(this);
	}
	
	@Override
	public void setProgressIndicator(boolean active) {
		mProgressBar.setVisibility(active?View.VISIBLE:View.GONE);
		if(active){showSnackbar();}
	}
	
	@Override
		public void showPhotos(List<PhotoItem> photos){
		mPhotoItems = photos;
		initRecyclerView();
	}
	
	@Override
	public void showEmptyPhotos() {
		
	}
	
	@Override
	public void showPhotoDetailUi(PhotoItem requestedPhotoItem) {
		PhotoItemDetailFragment detailFragment = new PhotoItemDetailFragment();
		Bundle arguments = new Bundle();
		arguments.putString(URL, requestedPhotoItem.getUserImageURL());
		detailFragment.setArguments(arguments);
		detailFragment.show(getFragmentManager(),detailFragment.getClass().getSimpleName());
	}
	private void showSnackbar(){
		Snackbar.make(getView(), mRefreshMessage, Snackbar.LENGTH_LONG)
				.show();
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()){
			case R.id.action_refresh:
				/*mPresenter.loadPhotoItems(true, getUrlParams());*/
				mLayoutManagerType = TWOWAY;
				initRecyclerView();
				break;
			case R.id.action_search_bug:
				mSearchQuery = BUG;
				break;
			case R.id.action_search_flower:
				mSearchQuery = FLOWER;
				break;
			case R.id.action_search_car:
				mSearchQuery = CAR;
				break;
		}
		mPresenter.loadPhotoItems(true, getUrlParams());
		return super.onOptionsItemSelected(item);
	}

    @Override
    public void onNavigationItemClicked(int itemId) {
        switch (itemId){
            case R.id.nav_list:
                mLayoutManagerType = LINEAR;
                break;
            case R.id.nav_grid:
                mLayoutManagerType = GRID;
                break;
            case R.id.nav_two_way:
                mLayoutManagerType = TWOWAY;
                break;
            case R.id.nav_load_more:
            mCurrentPage= mCurrentPage<3? ++mCurrentPage:1;
                break;
            case R.id.nav_flower:
                mSearchQuery= FLOWER;
                break;
            case R.id.nav_car:
                mSearchQuery= CAR;
                break;
            case R.id.nav_bug:
                mSearchQuery= BUG;
                break;
        }
        mPresenter.loadPhotoItems(true,getUrlParams());
            Toast.makeText(getActivity(),"Search Query is " + mSearchQuery ,Toast.LENGTH_LONG).show();
    }

    private HashMap<String,String> getUrlParams(){
		String appKey = getString(R.string.photo_search_app_key);
		String imageType = PHOTO;
		HashMap<String,String> params = new HashMap<>();
		params.put(PhotoSearchConstants.KEY, appKey);
		params.put(PhotoSearchConstants.QUERY, mSearchQuery);
		params.put(PhotoSearchConstants.PER_PAGE, Integer.toString(mPerPage));
        params.put(PhotoSearchConstants.PAGE, Integer.toString(mCurrentPage));
		params.put(PhotoSearchConstants.IMAGE_TYPE, imageType);
		return params;
	}

	private void initRecyclerView(){
		mRecyclerView.removeAllViews();
		switch (mLayoutManagerType){
			case GRID:
				mRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 5));
				break;
			case TWOWAY:
				mRecyclerView.setLayoutManager(new TwoWayLayoutManager());
				break;
			case LINEAR:
				mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
				break;
		}
		mRecyclerView.setAdapter(new PhotoSearchAdapter(getActivity(), mPhotoItems, mPresenter, mLayoutManagerType));
	}
}
