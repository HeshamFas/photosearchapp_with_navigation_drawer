package photosearch.heshamfas.com.photosearchapplication.contents;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

import java.util.List;

import photosearch.heshamfas.com.photosearchapplication.PhotoSearchApplication;
import photosearch.heshamfas.com.photosearchapplication.R;
import photosearch.heshamfas.com.photosearchapplication.communicator.PhotoSearchContract;
import photosearch.heshamfas.com.photosearchapplication.communicator.PhotoSearchPresenter;
import photosearch.heshamfas.com.photosearchapplication.data.PhotoItem;
import photosearch.heshamfas.com.photosearchapplication.fragment.PhotoSearchActivityFragment;

/**
 * Created by Hesham Fas on 3/11/2017.
 */

 public class PhotoSearchAdapter extends RecyclerView.Adapter<PhotoSearchAdapter.PhotoViewHolder> {
    private Context context;
    private List<PhotoItem> mPhotoItems;
	private ImageLoader mImageLoader = PhotoSearchApplication.getInstance().getImageLoader();
	private PhotoSearchContract.UserActionsListener mPresenter;
    private PhotoSearchActivityFragment.LayoutManagerType mLayoutManagerType;
   
	public PhotoSearchAdapter(Context context, List<PhotoItem> photoItems, PhotoSearchPresenter presenter, PhotoSearchActivityFragment.LayoutManagerType type){
        this.context = context;
        this.mPhotoItems = photoItems;
		this.mPresenter = presenter;
        mLayoutManagerType = type;
    }

    @Override
    public int getItemViewType(int position) {
       return mLayoutManagerType.ordinal();
    }

    @Override
    public PhotoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        PhotoViewHolder holder = null;
        switch (viewType){
            case 2:
                view = LayoutInflater.from(context).inflate(R.layout.photo_list_item,null);
                holder =new LinearViewHolder(view);
                break;
            default:
                view = LayoutInflater.from(context).inflate(R.layout.photo_grid_item,null);
                holder =new  GridViewHolder(view);
                break;
        }
        return holder;
    }

    @Override
    public void onBindViewHolder(PhotoViewHolder holder, int position) {
	    holder.bindView(mPhotoItems.get(position));
    }

    @Override
    public int getItemCount() {
        return mPhotoItems.size();
    }
    
    abstract class PhotoViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
	    TextView itemData;
	    NetworkImageView thumbImage;
	    PhotoItem photoItem;
	    public PhotoViewHolder(View itemView) {
		    super(itemView);
		    itemView.setOnClickListener(this);
	    }
	
	    @Override
	    public void onClick(View v) {
			mPresenter.openPhotoDetails(photoItem);
	    }
	    protected abstract void bindView(PhotoItem data);
	    
    }

   class GridViewHolder extends PhotoViewHolder{
         GridViewHolder(View itemView) {
            super(itemView);
                itemData = (TextView) itemView.findViewById(R.id.tv_item);
	            thumbImage = (NetworkImageView) itemView.findViewById(R.id.niv_photo_thumb);
        }
	
	    @Override
	    protected void bindView(PhotoItem data) {
		   this.photoItem = data;
		    itemData.setText((data.getPhotoId()));
		    thumbImage.setErrorImageResId(R.drawable.downloading);
		    thumbImage.setImageUrl(data.getPreviewURL(),mImageLoader);
		    itemView.setOnClickListener(this);
	    }
    }
    class LinearViewHolder extends PhotoViewHolder{
        LinearViewHolder(View itemView) {
            super(itemView);
            itemData = (TextView) itemView.findViewById(R.id.tv_item_id);
            thumbImage = (NetworkImageView) itemView.findViewById(R.id.niv_photo_thumb);
        }
        @Override
        protected void bindView(PhotoItem data) {
            this.photoItem = data;
            itemData.setText((data.getPhotoId()));
            thumbImage.setErrorImageResId(R.mipmap.ic_launcher);
            thumbImage.setImageUrl(data.getPreviewURL(),mImageLoader);
            itemView.setOnClickListener(this);
        }
    }
}
