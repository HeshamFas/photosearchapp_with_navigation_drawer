package photosearch.heshamfas.com.photosearchapplication.communicator;

/**
 * Created by hf731n on 3/14/2017.
 */

public interface SideMenuItemClickedListener {
    void onNavigationItemClicked(int itemId);
}
