package photosearch.heshamfas.com.photosearchapplication.data;

import java.util.List;

/**
 * Created by Hesham on 3/11/2017.
 */

public class PhotoSearchResponse {
	private boolean ready;
	private List<PhotoItem> photoItems;
	public boolean isReady() {
		return photoItems!=null&&photoItems.size()>0;
	}

	public List<PhotoItem> getPhotoItems() {
		return photoItems;
	}
	public void setPhotoItems(List<PhotoItem> photoItems) {
		this.photoItems = photoItems;
	}
}
