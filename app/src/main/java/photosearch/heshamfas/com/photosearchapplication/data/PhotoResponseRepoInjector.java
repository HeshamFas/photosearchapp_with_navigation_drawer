package photosearch.heshamfas.com.photosearchapplication.data;

import android.support.annotation.NonNull;
import android.util.Log;

import photosearch.heshamfas.com.photosearchapplication.PhotoSearchApplication;
import photosearch.heshamfas.com.photosearchapplication.service.PhotoRepoInjectorCallBack;
import photosearch.heshamfas.com.photosearchapplication.service.PhotoSearchServiceAPI;
import photosearch.heshamfas.com.photosearchapplication.service.PhotoSearchServiceCall;


/**
 * Created by Hesham on 3/11/2017.
 */
public class PhotoResponseRepoInjector implements PhotoSearchServiceAPI {

	PhotoRepoInjectorCallBack mInjectorCallBack;

	public PhotoResponseRepoInjector(@NonNull PhotoRepoInjectorCallBack callBack) {
		mInjectorCallBack = callBack;
	}

	public void launchPhotoSearchServiceCall(String url) {
		new PhotoSearchServiceCall(this).execute(url);
		Log.d("Hello", this.toString() + "calling promotion service call");
	}

	@Override
	public void onSuccess(PhotoSearchResponse repository) {
		mInjectorCallBack.onPhotoRepoReceived(repository);
		Log.d("Hello", this.toString() + "onSuccess");
		PhotoSearchApplication.getInstance().storeLastFreshRepository(repository);
	}

	@Override
	public void onFailure() {
		mInjectorCallBack.onPhotoRepoReceived(PhotoSearchApplication.getInstance().getCachedPromoRepo());
	}
}
