package photosearch.heshamfas.com.photosearchapplication.data;

/**
 * Created by Hesham on 3/11/2017.
 */

public class PhotoItem {
	
	public String getPageURL() {
		return pageURL;
	}
	
	public void setPageURL(String pageURL) {
		this.pageURL = pageURL;
	}
	
	String pageURL ="";
	
	public String getPreviewURL() {
		return previewURL;
	}
	
	public void setPreviewURL(String previewURL) {
		this.previewURL = previewURL;
	}
	
	public String getPostedBy() {
		return postedBy;
	}
	
	public void setPostedBy(String postedBy) {
		this.postedBy = postedBy;
	}
	
	public String getPhotoId() {
		return PhotoId;
	}
	
	public void setPhotoId(String photoId) {
		PhotoId = photoId;
	}
	
	public String getUserImageURL() {
		return userImageURL;
	}
	
	public void setUserImageURL(String userImageURL) {
		this.userImageURL = userImageURL;
	}
	
	String previewURL = "";
	        String postedBy ="" ;
			String PhotoId = "";
			String userImageURL = "";
			
}
