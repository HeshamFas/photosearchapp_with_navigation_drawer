package photosearch.heshamfas.com.photosearchapplication;

import android.app.Application;
import android.content.Context;
import android.support.annotation.NonNull;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;

import photosearch.heshamfas.com.photosearchapplication.data.PhotoSearchResponse;
import photosearch.heshamfas.com.photosearchapplication.service.BitmapLruCache;

/**
 * Created by Hesham on 3/11/2017.
 */

public class PhotoSearchApplication extends Application {
	
	private static PhotoSearchApplication instance;
	public static boolean online = true;
	private  String mainPhotoSourceUrl;
	private PhotoSearchResponse mPhotoSearchResponse;
	private static ImageLoader mImageLoader;
	/**
	 * 20% of the heap goes to image cache. Stored in kiloibytes.
	 */
	private static final int IMAGE_CACHE_SIZE_KB = (int) (Runtime.getRuntime().maxMemory() / 1024 / 5);
	
	@Override
	protected void attachBaseContext(Context base) {
		super.attachBaseContext(base);
		instance = this;
	}
	public static PhotoSearchApplication getInstance(){
		return instance;
	}
	
	@Override
	public void onCreate() {
		super.onCreate();
		mainPhotoSourceUrl = getResources().getString(R.string.main_photo_search_url);
	}
	
	public  String getMainSearchURL(){
		return mainPhotoSourceUrl;
	}
	
	public  void storeLastFreshRepository(@NonNull PhotoSearchResponse repository){
		mPhotoSearchResponse = repository;
	}
	
	public PhotoSearchResponse getCachedPromoRepo (){
		if(mPhotoSearchResponse!= null){
			return mPhotoSearchResponse;
		}else {
			return new PhotoSearchResponse();
		}
	}
	
	public ImageLoader getImageLoader() {
		if (mImageLoader == null) {
			mImageLoader = new ImageLoader(Volley.newRequestQueue(this), new BitmapLruCache(IMAGE_CACHE_SIZE_KB));
		}
		return mImageLoader;
	}
}
