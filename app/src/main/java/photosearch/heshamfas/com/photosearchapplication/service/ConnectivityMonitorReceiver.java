package photosearch.heshamfas.com.photosearchapplication.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import java.util.ArrayList;

/**
 * Created by Hesham on 5/14/2016.
 */
public class ConnectivityMonitorReceiver extends BroadcastReceiver {
   private static ArrayList<ConnectivityChangedListener> mListeners = new ArrayList<>();
    public static boolean isConnected = true;
    public interface ConnectivityChangedListener{
        void onConnectivityChanged(boolean isConnected);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        ConnectivityManager connectivityManager = (ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE );
        NetworkInfo activeNetInfo = connectivityManager.getActiveNetworkInfo();
        isConnected = activeNetInfo != null && activeNetInfo.isConnectedOrConnecting();
        notifyListeners(isConnected);
    }
    public static  void addListener(ConnectivityChangedListener listener){
        mListeners.add(listener);
    }

    public static boolean isConnected (){
            return isConnected;
    }
    public static void removeListener(ConnectivityChangedListener listener){
        mListeners.remove(listener);
    }

    private static void notifyListeners(boolean isConnected){
        for (ConnectivityChangedListener listener: mListeners) {
            listener.onConnectivityChanged(isConnected);
        }
    }
}

