package photosearch.heshamfas.com.photosearchapplication.service;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import photosearch.heshamfas.com.photosearchapplication.data.PhotoItem;
import photosearch.heshamfas.com.photosearchapplication.data.PhotoSearchResponse;

/**
 * Created by Hesham on 3/11/2017.
 */
public class PhotoSearchParser {

    private static final String HITS = "hits";
    private static final String PHOTO_THUMB_URL="previewURL";
    private static final String PHOTO_IMAGE_URL="webformatURL";
    private static final String PHOTO_ID = "id";


    public static PhotoSearchResponse parse(InputStream is) throws IOException,
		    JSONException {
        PhotoSearchResponse photoSearchResponse = new PhotoSearchResponse();
        ArrayList<PhotoItem> photoItems = new ArrayList<>();
        PhotoItem photoItem = null;
        String result = convertStreamToString(is);
        if (result != null) {
            
            JSONObject mainResponse = new JSONObject(result);
            JSONArray photoItemArray = mainResponse.getJSONArray(HITS);
            if(photoItemArray!= null ){
                for (int i = 0; i < photoItemArray.length(); i++) {
                    JSONObject tempObject = photoItemArray.getJSONObject(i);
                    if (tempObject != null) {
                        photoItem = new PhotoItem();
                        if(tempObject.has(PHOTO_THUMB_URL)){
                            photoItem.setPreviewURL(tempObject.getString(PHOTO_THUMB_URL));
                        }
                        if(tempObject.has(PHOTO_IMAGE_URL)){
                            photoItem.setUserImageURL(tempObject.getString(PHOTO_IMAGE_URL));
                        }
                        if(tempObject.has(PHOTO_ID)){
                            photoItem.setPhotoId(tempObject.getString(PHOTO_ID));
                        }
                       // promotions.add(promotionItem);
                    }
                    photoItems.add(photoItem);
                }
                photoSearchResponse.setPhotoItems(photoItems);
            }else {
               //TODO call failed callback
            }
            //promotionsRepository.setPromotions(promotions);

        }else {
            // TODO call failed callback
            //PromotionsRepository.setIsSuccessfull(false);
        }

        return photoSearchResponse;
    }


    public static String convertStreamToString(InputStream is) throws IOException {
        StringBuilder sb = new StringBuilder();
        if (is != null) {
            BufferedReader reader = new BufferedReader(new InputStreamReader(is));
            String line = null;
            try {
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
            } finally {
                is.close();
            }
        }
        return sb.toString();
    }

}
