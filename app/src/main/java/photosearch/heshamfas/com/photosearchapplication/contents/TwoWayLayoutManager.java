package photosearch.heshamfas.com.photosearchapplication.contents;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by Hesham Fas on 2/8/2017.
 */

    public class TwoWayLayoutManager extends RecyclerView.LayoutManager {
    private static String TAG = TwoWayLayoutManager.class.getSimpleName();
    int mVisibleColumns=0;
    private int mDefaultChildDecoratedWidth;
    private int mDefaultChildDecoratedHight;

    @Override
    public RecyclerView.LayoutParams generateDefaultLayoutParams() {
        return new RecyclerView.LayoutParams(RecyclerView.LayoutParams.WRAP_CONTENT, RecyclerView.LayoutParams.WRAP_CONTENT);
    }

/*    @Override
    public boolean supportsPredictiveItemAnimations() {
        return true;
    }*/

    @Override
    public void onLayoutChildren(RecyclerView.Recycler recycler, RecyclerView.State state) {
        super.onLayoutChildren(recycler, state);
        if (state.isPreLayout()) {
            return;
        }
        //Assuming all children are same size
        View scrap = recycler.getViewForPosition(0);
        addView(scrap);
        measureChildWithMargins(scrap,0,0);
        mDefaultChildDecoratedWidth = getDecoratedMeasuredWidth(scrap);
        mDefaultChildDecoratedHight = getDecoratedMeasuredHeight(scrap);
        removeAndRecycleView(scrap,recycler);
        fillVisibleChildren(recycler);
    }

    private void fillVisibleChildren(RecyclerView.Recycler recycler) {
        //before we layout child views, we first scrap all currently attached views
        detachAndScrapAttachedViews(recycler);

        //layoutInfo is a rect[], each element contains coordinates for a view
        mVisibleColumns =getTotalColumnCount();
        int viewLeft = 0;
        int top = 0;
        int right = 0;
        int bottom = 0;
       // int columns = 10;
        int rows = getItemCount() / mVisibleColumns +1;
        View view = null;
        for (int i = 0; i < rows; i++) {
            for (int x = 0; x < mVisibleColumns; x++) {
                int position = (i * mVisibleColumns) + x;
                if (position < getItemCount()) {
                    view = recycler.getViewForPosition(position);
                    view.setVisibility(View.VISIBLE);
                    addView(view);
                    measureChildWithMargins(view, 0, 0);
                    right += getDecoratedMeasuredWidth(view);
                    if(position == 0){
                        bottom+=getDecoratedMeasuredHeight(view);
                    }
                    layoutDecorated(view, viewLeft, top, right, bottom);
                /*right += *//*view.getWidth();*//*getDecoratedMeasuredWidth(view);*/
                   /* if(position%7 == 0){
                        removeAndRecycleView(view,recycler);
                    }*/
                    viewLeft += /*view.getWidth()*/ getDecoratedMeasuredWidth(view);
                    //logMeasures(view);
                } else {
                    return;
                }
            }
            viewLeft = 0;
            top += getDecoratedMeasuredHeight(view);
            bottom += getDecoratedMeasuredHeight(view);
        }
    }

    @Override
    public boolean canScrollHorizontally() {
        return true;
    }
    @Override
    public int scrollHorizontallyBy(int dx, RecyclerView.Recycler recycler, RecyclerView.State state) {
       // dx>0 scrolling left;
        //dx<0 scrolling right;
        int velosity = 2;
        View leftView = getChildAt(0);
        View rightView = getChildAt(mVisibleColumns -1);
        int decoratedLeft=getDecoratedLeft(leftView);
        int decoratedRight = getDecoratedRight(rightView);
        int width = getWidth();
        int viewSpan = decoratedRight- decoratedLeft;
        int horizontalScrollDistance;
        if(viewSpan<= getWidth()
                || (decoratedRight<= getWidth()&& dx>=0)
                || (decoratedLeft>= 0) && dx <= 0){
            return 0;
        }else if(decoratedLeft <0 && dx<0) {
            /*
            * if the left edge is off the screen
            * but the user scrolls distance is smaller than
            * the offset of the left edge then we scroll only
            * the amount of user scrolling
            * */
                horizontalScrollDistance = Math.max(dx,decoratedLeft);

            /*scrollDistance = decoratedLeft;*/
        }else if(decoratedRight > getWidth() &&dx >0){
            int rightOffset = decoratedRight - getWidth();
                horizontalScrollDistance = Math.min(rightOffset,dx) ;
            }else {
            horizontalScrollDistance = dx;
        }
        offsetChildrenHorizontal(-horizontalScrollDistance);

 /*       for( int i = 0; i<getChildCount() ; i+=mVisibleColumns){
            View view = recycler.getViewForPosition(i);
            if(getDecoratedRight(view)< 0 ){
                removeAndRecycleView(view,recycler);
            }
        }*/
        /*requestLayout();*/
        return -horizontalScrollDistance ;
    }
    @Override
    public boolean canScrollVertically() {
        return true;
    }

    @Override
    public int scrollVerticallyBy(int dy, RecyclerView.Recycler recycler, RecyclerView.State state) {
       //remember going up will dy goes higher + // going down dy goes lower -
        int velosity = 2;
        View topView = getChildAt(0);
        View bottomView = getChildAt(getChildCount()-1);
        final int height = getHeight();
        final int decoratedTop = getDecoratedTop(topView);
        final int decoratedBottom = getDecoratedBottom(bottomView);
        int verticalScrollDistance;
        final int viewSpan = getDecoratedBottom(bottomView)-getDecoratedTop(topView) ;
        if(viewSpan<= getHeight()
                || (getDecoratedTop(topView)>= 0 && dy<0)
                || ((getDecoratedBottom(bottomView)) <= getHeight() && dy > 0)){
            return 0;}
/*
* if top  of grid is above the edge of the layout
* and the user is scrolling down
* *choose the lower distance which is the higher negative number
*
* /*/
        else if(decoratedTop < 0 && dy <0 ){
            verticalScrollDistance = Math.max(decoratedTop, dy);
        }
        /*
        * if bottom  of grid is below the edge of the layout
        * and the user is scrolling up
        * choose the lower distance
        * */
        else if(decoratedBottom > height && dy > 0){
            int bottomOffset = decoratedBottom - height;
            verticalScrollDistance = Math.min(bottomOffset, dy);
        }else{
            verticalScrollDistance = dy;
        }
        offsetChildrenVertical(-verticalScrollDistance);
        return (-verticalScrollDistance);
    }

    private void logMeasures(View view) {
        ViewGroup.LayoutParams params = view.getLayoutParams();
        int width = params.width;
        int height = params.height;
        Log.d(TAG,
                "Child count " + getChildCount()
                        + " RecyclerViewWidth = " + getWidth()
                        + " Decorated Child width = " + getDecoratedMeasuredWidth(view)
                        + " width = " + view.getWidth()
                        + " Decorated child hight = " + getDecoratedMeasuredHeight(view)
                        + " height = " + view.getHeight()
                        + " decoracted  left = " + getDecoratedLeft(view)
                        + " Decorated top = " + getDecoratedTop(view)
                        + " decorated right = " + getDecoratedRight(view)
                        + " decorated bottom = " + getDecoratedBottom(view)

        );
    }
    private int getTotalColumnCount() {
        //Lol.dDev(Lol.HESHAM,TAG, "getTotalColumnCount");
       mVisibleColumns = (int)Math.ceil(getWidth()/(mDefaultChildDecoratedWidth * 1.0))+1;
        /*
        * this is to insure that when total items will not fill one row
        * then the total columns will be the total items
        *Hesham
         *  */
        if (getItemCount() < mVisibleColumns) {
            return getItemCount();
        }
        return mVisibleColumns;
    }
    @Override
    public void onScrollStateChanged(int state) {
        super.onScrollStateChanged(state);
    }
}
