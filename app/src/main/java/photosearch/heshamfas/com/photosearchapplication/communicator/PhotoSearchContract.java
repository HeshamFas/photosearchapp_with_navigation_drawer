package photosearch.heshamfas.com.photosearchapplication.communicator;

import android.support.annotation.NonNull;

import java.util.List;
import java.util.Map;
import photosearch.heshamfas.com.photosearchapplication.data.PhotoItem;

/**
 * Created by Hesham on 3/11/2017.
 */
public interface PhotoSearchContract {
    interface View {

        void setProgressIndicator(boolean active);
        void showPhotos(List<PhotoItem> photos);
        void showEmptyPhotos();
        void showPhotoDetailUi(PhotoItem requestedPhoto);
    }

    interface UserActionsListener {

        void loadPhotoItems(boolean forceUpdate, Map<String, String> params);
        void openPhotoDetails(@NonNull PhotoItem requestedPhoto);
    }

}
